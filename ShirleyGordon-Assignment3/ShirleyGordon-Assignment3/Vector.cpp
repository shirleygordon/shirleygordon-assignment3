#include "Vector.h"

/*
Constructor function for Vector object.
Input: size of vector.
Output: none.
*/
Vector::Vector(int n)
{
	unsigned int i = 0;

	// If n is smaller than 2, change it to 2.
	if (n < MIN_SIZE)
	{
		n = MIN_SIZE;
	}

	// Allocate memory for array and initialize it with 0s.
	this->_elements = new int[n];
	for (i = 0; i < n; i++)
	{
		this->_elements[i] = 0;
	}
	
	this->_size = EMPTY;
	this->_capacity = n;
	this->_resizeFactor = n;
}

/*
Destructor function for Vector object.
Input: none.
Output: none.
*/
Vector::~Vector()
{
	delete[] this->_elements;
	this->_elements = nullptr;
}

/*
Function returns the size of the vector.
Input: none.
Output: size of the vector.
*/
int Vector::size() const
{
	return this->_size;
}

/*
Function returns the capacity of the vector.
Input: none.
Output: capacity of the vector.
*/
int Vector::capacity() const
{
	return this->_capacity;
}

/*
Function returns the resize factor of the vector.
Input: none.
Output: resize factor of the vector.
*/
int Vector::resizeFactor() const
{
	return this->_resizeFactor;
}

/*
Function returns whether the vector is empty or not.
Input: none.
Output: true if the vector is empty, false otherwise.
*/
bool Vector::empty() const
{
	return this->_size == EMPTY;
}

/*
Function creates new array with the given capacity and copies the old array into it.
Then, it makes the old _elements pointer point to it.
Input: new capacity.
Output: none.
*/
void Vector::createNewArray(const unsigned int newCapacity)
{
	int* newArr = nullptr;
	unsigned int i = 0;

	newArr = new int[newCapacity]; // Allocate memory for a new array.

	// Copy old array into the new one
	for (i = 0; i < this->_size; i++)
	{
		newArr[i] = this->_elements[i];
	}

	this->_capacity = newCapacity; // Increase the capacity

	// Initialize all extra spaces in the array with 0s
	for (i = this->_size; i < this->_capacity; i++)
	{
		newArr[i] = 0;
	}

	delete[] this->_elements; // Deallocate memory of old array
	this->_elements = newArr; // Set the pointer to the new array
}

/*
Function adds element at the end of the vector.
Input: value to add.
Output: none.
*/
void Vector::push_back(const int& val)
{
	int* newArr = nullptr;
	unsigned int i = 0;

	if (this->_size == this->_capacity) // Vector is full.
	{
		createNewArray(this->_capacity + this->_resizeFactor); // Enlarge the array
	}

	this->_elements[this->_size] = val; // Add new element to the end of the vector
	this->_size++; // Increase size
}

/*
Function removes and returns the last element of the vector.
Input: none.
Output: the last element of the vector, or -9999 if the vector is empty.
*/
int Vector::pop_back()
{
	int poppedElement = POP_BACK_ERR;

	if (this->empty()) // If the vector is empty, print error message
	{
		cerr << "error: pop from empty vector" << endl;
	}
	else // Save the last element in order to return it, and then remove it from the vector.
	{
		poppedElement = this->_elements[this->_size - 1];
		this->_elements[this->_size - 1] = 0;
		this->_size--;
	}

	return poppedElement;
}

/*
Function changes the capacity to at least n.
Input: new capacity.
Output: none.
*/
void Vector::reserve(int n)
{
	int* newArr = nullptr;
	unsigned int i = 0, multiplyBy = 1;

	if (this->_capacity < n)
	{
		n = n - this->_capacity; // Calculate by how much the array needs to be enlarged
		multiplyBy = n / this->_resizeFactor; // Calculate how many times to multiply the resizeFactor
		
		if (n % this->_resizeFactor != 0) // If n isn't a multiple of resizeFactor, we'll need to make the array capacity larger than n.
		{
			multiplyBy++;
		}

		createNewArray(this->_capacity + this->_resizeFactor * multiplyBy); // Enlarge the array
	}
}

/*
Function resizes the vector so that it contains n elements.
Input: new vector size.
Output: none.
*/
void Vector::resize(int n)
{
	if(n <= this->_capacity) // If n is smaller or equal to the current capacity of the array:
	{
		this->_size = n; // The size of the array will now be n.
	}
	else if(n > this->_capacity) // If n is larger than the current capacity of the array:
	{
		this->reserve(n); // Enlarge the array to at least n capacity.
		this->_size = n; // The size of the array will now be n.
	}
}

/*
Function assigns a new value to all elements of the vector that are accessible,
replacing its current contents.
Input: new value.
Output: none.
*/
void Vector::assign(int val)
{
	unsigned int i = 0;

	// Assign val to all elements of the vector.
	for (i = 0; i < this->_size; i++)
	{
		this->_elements[i] = val;
	}
}

/*
Function resizes the vector so that it contains n elements,
and assigns a new value to all elements of the vector that are accessible,
replacing its current contents.
Input: new vector size, value to assign.
Output: none.
*/
void Vector::resize(int n, const int& val)
{
	this->resize(n); // Resize the vector.
	this->assign(val); // Assign a new value to all elements which are accessible.
}

/*
Copy constructor function for vector.
Input: reference to the vector to copy.
Output: none.
*/
Vector::Vector(const Vector& other)
{
	unsigned int i = 0;

	// Copy all static memory variables into the vector.
	this->_capacity = other._capacity;
	this->_size = other._size;
	this->_resizeFactor = other._resizeFactor;

	// Allocate memory for new elements array.
	this->_elements = new int[this->_capacity];

	// Deep copy elements array into the vector.
	for (i = 0; i < this->_capacity; i++)
	{
		this->_elements[i] = other._elements[i];
	}
}

/*
Function deep copies other vector object into this object.
Input: other vector object reference.
Output: this vector object reference.
*/
Vector& Vector::operator=(const Vector& other)
{	
	unsigned int i = 0;

	if (this != &other) // If the vector isn't trying to copy itself:
	{
		// Deallocate old memory.
		delete[] this->_elements;
		this->_elements = nullptr;
		
		// Copy all static memory variables into the vector.
		this->_capacity = other._capacity;
		this->_size = other._size;
		this->_resizeFactor = other._resizeFactor;

		// Allocate memory for new elements array.
		this->_elements = new int[this->_capacity];

		// Deep copy elements array into the vector.
		for (i = 0; i < this->_capacity; i++)
		{
			this->_elements[i] = other._elements[i];
		}
	}

	return *this;
}

/*
Function returns reference to the n'th element in the vector.
Input: index in the vector.
Output: the element at index n,
or the first element if n is larger than the size of the vector.
*/
int& Vector::operator[](int n) const
{
	if (n > this->_size - 1 || n < 0) // If n is larger than the size of the vector, or smaller than 0:
	{
		n = 0; // Function will return the first element in the vector.
		cerr << "Error: index exceeds vector boundaries." << endl; // Print error message.
	}

	return this->_elements[n];
}

/*
Function adds the values of two vectors and returns a new vector containing the results.
Input: reference to other vector.
Output: new vector containing the results.
*/
Vector Vector::operator+(const Vector& other) const
{
	Vector newVector(MIN_SIZE); // Create a new vector object.
	unsigned int i = 0;

	// Put results into the new vector.
	for (i = 0; i < this->_size && i < other._size; i++)
	{
		newVector.push_back(this->_elements[i] + other._elements[i]);
	}

	return newVector;
}

/*
Function subtracts the values of two vectors and returns a new vector containing the results.
Input: reference to other vector.
Output: new vector containing the results.
*/
Vector Vector::operator-(const Vector& other) const
{
	Vector newVector(MIN_SIZE); // Create a new vector object.
	unsigned int i = 0;

	// Put results into the new vector.
	for (i = 0; i < this->_size && i < other._size; i++)
	{
		newVector.push_back(this->_elements[i] - other._elements[i]);
	}

	return newVector;
}

/*
Function subtracts other vector from *this and returns *this.
Input: reference to other vector.
Output: reference to *this vector.
*/
Vector& Vector::operator-=(const Vector& other)
{
	unsigned int i = 0;

	// Subtract other vector elements from this vector elements.
	for (i = 0; i < this->_size && i < other._size; i++)
	{
		this->_elements[i] -= other._elements[i];
	}

	return *this;
}

/*
Function adds other vector to *this and returns *this.
Input: reference to other vector.
Output: reference to *this vector.
*/
Vector& Vector::operator+=(const Vector& other)
{
	unsigned int i = 0;

	// Add other vector elements to this vector elements.
	for (i = 0; i < this->_size && i < other._size; i++)
	{
		this->_elements[i] += other._elements[i];
	}

	return *this;
}

/*
Function formats vector fields into a structured output.
Input: output stream reference, vector reference.
Output: output stream reference.
*/
ostream& operator<<(ostream& output, const Vector& v)
{
	unsigned int i = 0;

	// Add all vector fields to output stream
	output << "Vector Info:\nCapacity is " << v._capacity << "\nSize is " << v._size;
	
	if (!v.empty()) // If vector isn't empty
	{
		output << "\nData is ";

		// Add all elements to output stream, separated by commas
		for (i = 0; i < v._size - 1; i++)
		{
			output << v[i] << ", ";
		}
		output << v[v._size - 1] << endl;
	}
	else // Vector is empty
	{
		output << "\nVector is empty." << endl;
	}

	return output;
}
